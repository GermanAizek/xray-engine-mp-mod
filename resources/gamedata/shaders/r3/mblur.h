#ifndef	MBLURDX10_H
#define MBLURDX10_H

/*#ifndef 	USE_MBLUR
float3 	mblur	(float2 UV, float3 pos, float3 c_original)	{ return c_original; }
#else//USE_MBLUR*/
#include "common.h"

uniform float4x4	m_current;
uniform float4x4	m_previous;
uniform float2 	m_blur;		// scale_x / 12, scale_y / 12

#define MBLUR_SAMPLES 	int(12)
#define MBLUR_CLAMP	float(0.001)

float3 	mblur	(float2 UV, float3 pos, float3 c_original)	{
	float4 	pos4		= float4	(pos,1.h);

	float4 	p_current	= mul	(m_current,	pos4);
	float4 	p_previous 	= mul	(m_previous,	pos4);
	float2 	p_velocity 	= m_blur * ( (p_current.xy/p_current.w)-(p_previous.xy/p_previous.w) );
		p_velocity	= clamp	(p_velocity,-MBLUR_CLAMP,+MBLUR_CLAMP);

	float3 	blurred 	= 	c_original	;
        	blurred 	+= 	s_image.Sample(smp_rtlinear, p_velocity * 1.h  + UV).rgb;
		blurred		+= 	s_image.Sample(smp_rtlinear, p_velocity * 2.h  + UV).rgb;
		blurred		+= 	s_image.Sample(smp_rtlinear, p_velocity * 3.h  + UV).rgb;
		blurred		+= 	s_image.Sample(smp_rtlinear, p_velocity * 4.h  + UV).rgb;
        	blurred 	+= 	s_image.Sample(smp_rtlinear, p_velocity * 5.h  + UV).rgb;
		blurred		+= 	s_image.Sample(smp_rtlinear, p_velocity * 6.h  + UV).rgb;
		blurred		+= 	s_image.Sample(smp_rtlinear, p_velocity * 7.h  + UV).rgb;
		blurred		+= 	s_image.Sample(smp_rtlinear, p_velocity * 8.h  + UV).rgb;
        	blurred 	+= 	s_image.Sample(smp_rtlinear, p_velocity * 9.h  + UV).rgb;
		blurred		+= 	s_image.Sample(smp_rtlinear, p_velocity * 10.h + UV).rgb;
		blurred		+= 	s_image.Sample(smp_rtlinear, p_velocity * 11.h + UV).rgb;
	return 	blurred/MBLUR_SAMPLES;
}
//#endif//USE_MBLUR

#endif